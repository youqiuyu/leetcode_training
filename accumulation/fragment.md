# Java零碎知识

#### 1. String的substring()方法
- beginIndex   the beginning index, inclusive.
- endIndex     the ending index, exclusive.
- the length of the substring is {endIndex-beginIndex}

#### 2. String, StringBuilder, StringBuffer
- String是字符串常量，是不可改变的，对该字符串操作都会产生一个新的字符串常量。
- StringBuffer和StringBuilder是字符串变量，每次操作是对本身的字符串操作.
- StringBuffer线程安全
- StringBuilder线程不安全，效率更高。
- 由于每次String是不可变对象，因此每一次都会操作都会产生新的String对象，然后再将指针指向新的String。所以对于那些需要经常需要改变的字符串，不建议用String，多线程下建议StringBuffer，单线程建议使用StringBuilder

#### 3.链表LinkedList的一些方法
- add()和offer()都是向队列中添加一个元素
- remove() 和 poll() 方法都是从队列中删除第一个元素
- element() 和 peek() 用于在队列的头部查询元素

#### 4. Arrays.fill()方法，用于批量赋值
    int[] lastOneIndex = new int[len];  
    Arrays.fill(lastOneIndex, 0);

#### 声明队列的两种方式
```
Deque<TreeNode> treeQueue = new ArrayDeque<>();
Queue<TreeNode> treeQueue = new LinkedList<>();
```
注意**poll()方法**可以直接把队首元素取出来并删掉