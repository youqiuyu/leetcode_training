/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package DFSandBFS.lc_617;

import java.util.LinkedList;
import java.util.Queue;

/*
给定两个二叉树，想象当你将它们中的一个覆盖到另一个上时，两个二叉树的一些节点便会重叠。
你需要将他们合并为一个新的二叉树。合并的规则是如果两个节点重叠，那么将他们的值相加作为节点合并后的新值，否则不为NULL 的节点将直接作为新二叉树的节点。

示例1:
输入:
	Tree 1                     Tree 2
          1                         2
         / \                       / \
        3   2                     1   3
       /                           \   \
      5                             4   7
输出:
合并后的树:
	     3
	    / \
	   4   5
	  / \   \
	 5   4   7
注意:合并必须从两个树的根节点开始。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/merge-two-binary-trees
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        TreeNode mergeTree = new TreeNode();
        if (root1 == null) {
            return root2;
        }
        if (root2 == null) {
            return root1;
        }
        mergeTree.val = root1.val + root2.val;
        mergeTree.left = mergeTrees(root1.left, root2.left);
        mergeTree.right = mergeTrees(root1.right, root2.right);
        return mergeTree;
    }

    public static TreeNode mergeTrees2(TreeNode root1, TreeNode root2) {
        if (root1 == null) return root2;
        if (root2 == null) return root1;
        root1.val = root1.val + root2.val;
        root1.left = mergeTrees2(root1.left, root2.left);
        root1.right = mergeTrees2(root1.right, root2.right);
        return root1;
    }

    public static void main(String[] args) {
        TreeNode tree15 = new TreeNode(5, null, null);
        TreeNode tree12 = new TreeNode(2, null, null);
        TreeNode tree13 = new TreeNode(3, tree15, null);
        TreeNode test1 = new TreeNode(1, tree13, tree12);

        TreeNode tree24 = new TreeNode(4, null, null);
        TreeNode tree27 = new TreeNode(7, null, null);
        TreeNode tree21 = new TreeNode(1, null, tree24);
        TreeNode tree23 = new TreeNode(3, null, tree27);
        TreeNode test2 = new TreeNode(2, tree21, tree23);

        TreeNode res = mergeTrees2(test1, test2);
        System.out.println("Finished");

    }

}
