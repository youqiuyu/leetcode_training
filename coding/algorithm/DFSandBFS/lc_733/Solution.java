package DFSandBFS.lc_733;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/*
有一幅以二维整数数组表示的图画，每一个整数表示该图画的像素值大小，数值在 0 到 65535 之间。
给你一个坐标(sr, sc)表示图像渲染开始的像素值（行 ，列）和一个新的颜色值newColor，让你重新上色这幅图像。
为了完成上色工作，从初始坐标开始，记录初始坐标的上下左右四个方向上像素值与初始坐标相同的相连像素点，接着再记录这四个方向上符合条件的像素点与他们对应四个方向上像素值与初始坐标相同的相连像素点，……，重复该过程。将所有有记录的像素点的颜色值改为新的颜色值。
最后返回经过上色渲染后的图像。

示例 1:
输入:
image = [[1,1,1],[1,1,0],[1,0,1]]
sr = 1, sc = 1, newColor = 2
输出: [[2,2,2],[2,2,0],[2,0,1]]
解析:
在图像的正中间，(坐标(sr,sc)=(1,1)),
在路径上所有符合条件的像素点的颜色都被更改成2。
注意，右下角的像素没有更改为2，
因为它不是在上下左右四个方向上与初始点相连的像素点。
注意:

image和image[0]的长度在范围[1, 50] 内。
给出的初始点将满足0 <= sr < image.length 和0 <= sc < image[0].length。
image[i][j]和newColor表示的颜色值在范围[0, 65535]内。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/flood-fill
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

// 三种实现亲手写一遍： ①栈 ②队列 ③递归
public class Solution {
    // 深度优先搜索：栈实现
    public static int[][] floodFill3(int[][] image, int sr, int sc, int newColor) {
        if (image[sr][sc] == newColor) {
            return image;
        }
        int[] dx = {-1, 0, 0, 1};
        int[] dy = {0, 1, -1, 0};
        int height = image.length;
        int width = image[0].length;
        int oldValue = image[sr][sc];
        Deque<int[]> deque = new LinkedList<>();
        deque.addFirst(new int[]{sr, sc});
        while (!deque.isEmpty()) {
            int[] top = deque.pollFirst();
            int row = top[0];
            int col = top[1];
            image[row][col] = newColor;
            for (int i = 0; i < 4; ++i) {
                int rowNew = row + dx[i];
                int colNew = col + dy[i];
                if (rowNew < height && rowNew >= 0 && colNew < width && colNew >= 0) {
                    if (image[rowNew][colNew] == oldValue) {
                        deque.addFirst(new int[]{rowNew, colNew});
                    }
                }
            }


//            if (row + 1 < height && image[row + 1][col] == oldValue) {
//                deque.addFirst(new int[]{row + 1, col});
//            }
//            if (row - 1 >= 0 && image[row - 1][col] == oldValue) {
//                deque.addFirst(new int[]{row - 1, col});
//            }
//            if (col + 1 < width && image[row][col + 1] == oldValue) {
//                deque.addFirst(new int[]{row, col + 1});
//            }
//            if (col - 1 >= 0 && image[row][col - 1] == oldValue) {
//                deque.addFirst(new int[]{row, col - 1});
//            }
        }
        return image;
    }

    // 广度优先搜索：队列实现
    public static int[][] floodFill4(int[][] image, int sr, int sc, int newColor) {
        if (image[sr][sc] == newColor) {
            return image;
        }
        int height = image.length;
        int width = image[0].length;
        int oldValue = image[sr][sc];
        Deque<int[]> deque = new LinkedList<>();
        deque.addLast(new int[]{sr, sc});
        image[sr][sc] = newColor;
        while (!deque.isEmpty()) {
            int[] top = deque.pollFirst();
            int row = top[0];
            int col = top[1];
            image[row][col] = newColor;
            if (row + 1 < height && image[row + 1][col] == oldValue) {
                deque.addLast(new int[]{row + 1, col});
            }
            if (row - 1 >= 0 && image[row - 1][col] == oldValue) {
                deque.addLast(new int[]{row - 1, col});
            }
            if (col + 1 < width && image[row][col + 1] == oldValue) {
                deque.addLast(new int[]{row, col + 1});
            }
            if (col - 1 >= 0 && image[row][col - 1] == oldValue) {
                deque.addLast(new int[]{row, col - 1});
            }
        }
        return image;
    }


    // 深度优先搜索 + 递归
    public static int[][] floodFill2(int[][] image, int sr, int sc, int newColor) {
        int oldColor = image[sr][sc];
        if (oldColor != newColor)
            dfs(image, sr, sc, image.length, image[0].length, oldColor, newColor);
        return image;
    }
    private static void dfs(int[][] image, int row, int col, int n, int m, int oldColor, int newColor) {
        //越界或者不等于oldColor，直接return
        if (row < 0 || row >= n || col < 0 || col >= m || image[row][col] != oldColor) {
            return;
        }
        image[row][col] = newColor;
        //上下左右
        dfs(image, row - 1, col, n, m, oldColor, newColor);
        dfs(image, row, col - 1, n, m, oldColor, newColor);
        dfs(image, row + 1, col, n, m, oldColor, newColor);
        dfs(image, row, col + 1, n, m, oldColor, newColor);
    }

    public static void main(String[] args) {
        //int[][] image = {{1,1,1},{1,1,0},{1,0,1}};
        int[][] image2 = {{0,0,0},{0,1,0}};
        int sr = 1, sc = 1, newColor = 2;
        floodFill3(image2, 1, 1, 2);
        System.out.println("Finished");
    }

}
