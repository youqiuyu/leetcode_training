/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package DFSandBFS.lc_994;

import java.util.Deque;
import java.util.LinkedList;

/*
在给定的网格中，每个单元格可以有以下三个值之一：

值0代表空单元格；
值1代表新鲜橘子；
值2代表腐烂的橘子。
每分钟，任何与腐烂的橘子（在 4 个正方向上）相邻的新鲜橘子都会腐烂。

返回直到单元格中没有新鲜橘子为止所必须经过的最小分钟数。如果不可能，返回-1。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/rotting-oranges
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static int orangesRotting(int[][] grid) {
        int time = 0, depth = grid.length, width = grid[0].length;
        boolean[][] visited = new boolean[depth][width];
        for (int i = 0; i < depth; ++i) {
            for (int j = 0; j < width; ++j) {
                visited[i][j] = false;
            }
        }
        for (int i = 0; i  < depth; ++i) {
            for (int j = 0; j < width; ++j) {
                if(grid[i][j] == 2) {
                    // 开始广度搜索
                    Deque<int[]> deque = new LinkedList<>();
                    deque.offerLast(new int[]{i, j});
                    while (!deque.isEmpty()) {
                        int loopSize = deque.size();
                        time++;
                        for (int k = 0; k < loopSize; k++) {
                            int[] top = deque.pollFirst();
                            int row = top[0];
                            int col = top[1];
                            if (row - 1 >= 0 && grid[row - 1][col] == 1 && !visited[row - 1][col]) {
                                visited[row - 1][col] = true;
                                deque.offerFirst(new int[]{row - 1, col});
                            }
                            if (row + 1 < depth && grid[row + 1][col] == 1 && !visited[row + 1][col]) {
                                visited[row + 1][col] = true;
                                deque.offerFirst(new int[]{row + 1, col});
                            }
                            if (col - 1 >= 0 && grid[row][col - 1] == 1 && !visited[row][col - 1]) {
                                visited[row][col - 1] = true;
                                deque.offerFirst(new int[]{row, col - 1});
                            }
                            if (col + 1 < width && grid[row][col + 1] == 1 && !visited[row][col + 1]) {
                                visited[row][col + 1] = true;
                                deque.offerFirst(new int[]{row, col + 1});
                            }
                        }
                    }
                }
            }
        }
        for (int i = 0; i < depth; ++i) {
            for (int j = 0; j < width; ++j) {
                if (grid[i][j] == 1) {
                    return -1;
                }
            }
        }
        return time;
    }

    public static void main(String[] args) {
        int[][] test1 = {
                {2,1,1},
                {1,1,0},
                {0,1,1}
        };
        int[][] test2 = {
                {2,1,1},
                {0,1,1},
                {1,0,1}
        };
        int[][] test3 = {
                {0,2}
        };
        System.out.println(orangesRotting(test1));
        System.out.println(orangesRotting(test2));
        System.out.println(orangesRotting(test3));
    }
}
