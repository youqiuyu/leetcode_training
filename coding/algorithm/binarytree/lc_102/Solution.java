package binarytree.lc_102;

import com.sun.jmx.remote.internal.ArrayQueue;

import java.util.*;


//二叉树按层遍历或者说深度优先遍历，是需要队列的
class Solution {

    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> treeQueue = new LinkedList<>();
        treeQueue.add(root);
        while (!treeQueue.isEmpty()) {
            List<Integer> currentLayer = new ArrayList<>();
            int currentSize = treeQueue.size();
            for (int i = 0; i < currentSize; ++i) {
                TreeNode top = treeQueue.poll();
                currentLayer.add(top.val);
                if (top.left != null) {
                    treeQueue.add(top.left);
                }
                if (top.right != null) {
                    treeQueue.add(top.right);
                }
            }
            res.add(currentLayer);
        }
        return res;
    }

    public static List<List<Integer>> levelOrder2(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Deque<TreeNode> treeQueue = new ArrayDeque<>();
        treeQueue.add(root);
        while (!treeQueue.isEmpty()) {
            List<Integer> currentLayer = new ArrayList<>();
            int currentSize = treeQueue.size();
            for (int i = 0; i < currentSize; ++i) {
                TreeNode top = treeQueue.pollFirst();
                currentLayer.add(top.val);
                if (top.left != null) {
                    treeQueue.addLast(top.left);
                }
                if (top.right != null) {
                    treeQueue.addLast(top.right);
                }
            }
            res.add(currentLayer);
        }
        return res;
    }

    public static void main(String[] args) {
        TreeNode test15 = new TreeNode(15, null, null);
        TreeNode test7 = new TreeNode(7, null, null);
        TreeNode test20 = new TreeNode(20, test15, test7);
        TreeNode test9 = new TreeNode(9, null, null);
        TreeNode root = new TreeNode(3, test9, test20);
        List<List<Integer>> result = levelOrder2(root);
        System.out.println("XXX");

    }

}
