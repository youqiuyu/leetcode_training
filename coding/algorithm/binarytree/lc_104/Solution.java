/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package binarytree.lc_104;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

/*
给定一个二叉树，找出其最大深度。
二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。
说明:叶子节点是指没有子节点的节点。

示例：
给定二叉树 [3,9,20,null,null,15,7]，

    3
   / \
  9  20
    /  \
   15   7
返回它的最大深度3 。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/maximum-depth-of-binary-tree
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    // bfs 队列
    public static int maxDepth2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Deque<TreeNode> deque = new LinkedList<>();
        int resMax = 0;
        deque.addFirst(root);
        while (!deque.isEmpty()) {
            resMax++;
            int size = deque.size();
            for (int i = 0; i < size; ++i) {
                TreeNode currentNode = deque.pollFirst();
                if (currentNode.left != null) {
                    deque.addLast(currentNode.left);
                }
                if (currentNode.right != null) {
                    deque.addLast(currentNode.right);
                }
            }
        }
        return resMax;
    }

    public static int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }

    public static void main(String[] args) {
        TreeNode left = new TreeNode(9, null, null);
        TreeNode right1 = new TreeNode(15, null, null);
        TreeNode right2 = new TreeNode(7, null, null);
        TreeNode right = new TreeNode(20, right1, right2);
        TreeNode test = new TreeNode(3, left, right);
        System.out.println(maxDepth(test));

    }

}
