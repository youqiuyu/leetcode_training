/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package binarytree.lc_105;

import java.util.HashMap;
import java.util.Map;

/*
从前序与中序遍历序列构造二叉树
给定一棵树的前序遍历 preorder 与中序遍历  inorder。请构造二叉树并返回其根节点。
示例 1:
Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
Output: [3,9,20,null,null,15,7]
示例 2:

Input: preorder = [-1], inorder = [-1]
Output: [-1]

提示:
1 <= preorder.length <= 3000
inorder.length == preorder.length
-3000 <= preorder[i], inorder[i] <= 3000
preorder和inorder 均无重复元素
inorder均出现在preorder
preorder保证为二叉树的前序遍历序列
inorder保证为二叉树的中序遍历序列

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */



// 该题的关键，1是递归，2是预处理hash表
public class Solution {
    public static TreeNode buildTree(int[] preorder, int[] inorder) {
        int n = preorder.length;
        // 构造哈希映射，帮助我们快速定位根节点
        Map<Integer, Integer> indexMap = new HashMap<Integer, Integer>();
        for (int i = 0; i < n; i++) {
            indexMap.put(inorder[i], i);
        }
        // pLeft -> pRight 表示一棵树在先序中的排列 pLeft pRight表示preorder中的下标
        // iLeft -> iRight 表示一棵树在中序中的排列 iLeft iRight表示inorder中的下标
        return build(preorder, inorder, 0, n - 1, 0, n - 1, indexMap);
    }

    private static TreeNode build(int[] preorder, int[] inorder, int pLeft, int pRight, int iLeft, int iRight, Map<Integer, Integer> indexMap) {
        if (pLeft == pRight) { // 只剩下一个元素，则就是根
            return new TreeNode(preorder[pLeft], null, null);
        }
        if (pRight < pLeft || iRight < iLeft) {
            return null;
        }
        TreeNode res = new TreeNode();
        int nodeValue = preorder[pLeft];
        res.val = nodeValue;
        // 在inorder中找到nodeValue
//        int split = iLeft - 1;
//        do {
//            split++;
//        } while (inorder[split] != nodeValue);
        int split = indexMap.get(nodeValue);
        // 那么，从iLeft到split - 1，就是左子树的中序遍历，从split + 1到iRight，就是右子树的中序遍历， 左子树的长度为 split - iLeft
        // 那么从pLeft + 1 到 pLeft + 1 + split - iLeft - 1 就是左子树的前序遍历，从 pLeft + 1 + split - iLeft到 pRight就是右子树的前序遍历
        res.left = build(preorder, inorder, pLeft + 1, pLeft + split - iLeft, iLeft, split - 1, indexMap);
        res.right = build(preorder, inorder, pLeft + 1 + split - iLeft, pRight, split + 1, iRight, indexMap);
        return res;
    }

    public static void main(String[] args) {
        int[] pre = {1, 2};
        int[] in = {1, 2};
        TreeNode result = buildTree(pre, in);
    }
}
