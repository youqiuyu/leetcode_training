/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package binarytree.lc_106;

import java.util.HashMap;
import java.util.Map;

/*
根据一棵树的中序遍历与后序遍历构造二叉树。

注意:
你可以假设树中没有重复的元素。

例如，给出

中序遍历 inorder =[9,3,15,20,7]
后序遍历 postorder = [9,15,7,20,3]
返回如下的二叉树：

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        int n = inorder.length;
        Map<Integer, Integer> indexMap = new HashMap<>();
        for (int i = 0; i < n; ++i) {
            indexMap.put(inorder[i], i);
        }
        return build(inorder, postorder, 0, n - 1, 0, n - 1, indexMap);
    }

    private TreeNode build(int[] inorder, int[] postorder, int iLeft, int iRight, int pLeft, int pRight, Map<Integer, Integer> indexMap) {
        if (iRight < iLeft || pRight < pLeft) {
            return null;
        }
        TreeNode res = new TreeNode(postorder[pRight]);
        int split = indexMap.get(postorder[pRight]);
        // 从iLeft到split - 1， 是左子树的中序， 从split + 1 到iRight,是右子树的中序
        // 从pLeft 到 pleft + split - iLeft - 1，是左子树的后序，从pleft + split - iLeft到iRight - 1，是右子树的后序
        res.left = build(inorder, postorder, iLeft, split - 1, pLeft, pLeft + split - iLeft - 1, indexMap);
        res.right = build(inorder, postorder, split + 1, iRight, pLeft + split - iLeft, pRight - 1, indexMap);
        return res;
    }
}
