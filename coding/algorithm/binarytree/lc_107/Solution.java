package binarytree.lc_107;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Solution {

    // 102题求个逆序
    public static List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> treeQueue = new LinkedList<>();
        treeQueue.add(root);
        while (!treeQueue.isEmpty()) {
            List<Integer> currentLayer = new ArrayList<>();
            int currentSize = treeQueue.size();
            for (int i = 0; i < currentSize; ++i) {
                TreeNode top = treeQueue.poll();
                currentLayer.add(top.val);
                if (top.left != null) {
                    treeQueue.add(top.left);
                }
                if (top.right != null) {
                    treeQueue.add(top.right);
                }
            }
            res.add(currentLayer);
        }
        List<List<Integer>> result = new ArrayList<>();
        for (int i = res.size() - 1; i >= 0; --i) {
            result.add(res.get(i));
        }
        return result;
    }

    // 链表插前面
    public static List<List<Integer>> levelOrderBottom2(TreeNode root) {
        List<List<Integer>> res = new LinkedList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> treeQueue = new LinkedList<>();
        treeQueue.add(root);
        while (!treeQueue.isEmpty()) {
            List<Integer> currentLayer = new ArrayList<>();
            int currentSize = treeQueue.size();
            for (int i = 0; i < currentSize; ++i) {
                TreeNode top = treeQueue.poll();
                currentLayer.add(top.val);
                if (top.left != null) {
                    treeQueue.add(top.left);
                }
                if (top.right != null) {
                    treeQueue.add(top.right);
                }
            }
            res.add(0, currentLayer);
        }
        return res;
    }

    public static void main(String[] args) {
        TreeNode test15 = new TreeNode(15, null, null);
        TreeNode test7 = new TreeNode(7, null, null);
        TreeNode test20 = new TreeNode(20, test15, test7);
        TreeNode test9 = new TreeNode(9, null, null);
        TreeNode root = new TreeNode(3, test9, test20);

        List<List<Integer>> result = levelOrderBottom2(root);
        System.out.println("XXX");

    }
}
