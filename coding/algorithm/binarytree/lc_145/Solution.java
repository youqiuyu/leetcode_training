/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package binarytree.lc_145;
import java.util.*;

public class Solution {
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        res.addAll(postorderTraversal(root.left));
        res.addAll(postorderTraversal(root.right));
        res.add(root.val);
        return res;
    }

    //迭代法 前序反转
    public List<Integer> postorderTraversal2(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Deque<TreeNode> deque = new LinkedList<>();
        deque.offerFirst(root);

        while (!deque.isEmpty()) {
            TreeNode curNode = deque.pollFirst();
            res.add(curNode.val);
            if (curNode.left != null) {
                deque.offerFirst(curNode.left);
            }
            if (curNode.right != null) {
                deque.offerFirst(curNode.right);
            }
        }
        Collections.reverse(res);
        return res;
    }

    // 迭代法 后序遍历
//    public List<Integer> postorderTraversal3(TreeNode root) {
//        List<Integer> res = new ArrayList<>();
//        if (root == null) {
//            return res;
//        }
//        Deque<TreeNode> deque = new LinkedList<>();
//        TreeNode curNode = root;
//        while (!deque.isEmpty() || curNode != null) {
//            while (curNode != null) {
//                deque.offerFirst(curNode);
//                if (curNode.right != null) {
//                    deque.offerFirst(curNode.right);
//                }
//            }
//
//        }
//    }
}
