/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package binarytree.lc_226;

/*
翻转一棵二叉树。
示例：
输入：
     4
   /   \
  2     7
 / \   / \
1   3 6   9

输出：
     4
   /   \
  7     2
 / \   / \
9   6 3   1

 */
public class Solution {
    public TreeNode invertTree(TreeNode root) {
        return root == null ? null : new TreeNode(root.val, invertTree(root.right), invertTree(root.left));
    }
}
