/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package binarytree.lc_94;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        res.addAll(inorderTraversal(root.left));
        res.add(root.val);
        res.addAll(inorderTraversal(root.right));
        return res;
    }

    public List<Integer> inorderTraversal2(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        TreeNode curNode = root;
        Deque<TreeNode> deque = new LinkedList<>();
        while (!deque.isEmpty() || curNode != null) {
            while (curNode != null) {
                deque.offerFirst(curNode);
                curNode = curNode.left;
            }
            TreeNode node = deque.pollFirst();
            res.add(node.val);
            if (node.right != null) {
                curNode = node.right;
            }
        }
        return res;
    }
}
