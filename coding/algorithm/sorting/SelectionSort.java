package sorting;

public class SelectionSort {
    public static void main(String[] args) {
        int[] arr = {4,5,7,1,9,2,3,8,6};
        for (int j = 0; j < arr.length - 1; ++j) {
            for (int k = j + 1; k < arr.length; ++k) {
                if (arr[k] < arr[j]) {
                    int tmp = arr[j];
                    arr[j] = arr[k];
                    arr[k] = tmp;
                }
            }
        }
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }
}
