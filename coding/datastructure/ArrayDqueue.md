# ArrayDeque类

#### 我们知道，Queue是队列，只能一头进，另一头出。  
#### 如果把条件放松一下，允许两头都进，两头都出，这种队列叫双端队列（Double Ended Queue），学名Deque。

### Java集合提供了接口Deque来实现一个双端队列，它的功能是：
- 既可以添加到队尾，也可以添加到队首；
- 既可以从队首获取，又可以从队尾获取。  
#### 我们来比较一下Queue和Deque出队和入队的方法：

|  操作               | Queue                     | 	Deque                        | 
|  :----:            | :----:                     |:----:                            |
| 添加元素到队尾      | add(E e) / offer(E e)      |addLast(E e) / offerLast(E e)     |
| 取队首元素并删除    | E remove() / E poll()      |E removeFirst() / E pollFirst()   |
| 取队首元素但不删除  | E element() / E peek()     |E getFirst() / E peekFirst()      |
| 添加元素到队首      | 无                         |addFirst(E e) / offerFirst(E e)   |
| 取队尾元素并删除    | 无                         |E removeLast() / E pollLast()     |
| 取队尾元素但不删除  | 无                         |E getLast() / E peekLast()        |

- Queue提供的add()/offer()方法在Deque中也可以使用，但是，使用Deque，最好不要调用offer()，而是调用offerLast()
- ArrayDeque不是线程安全的。 
- ArrayDeque不可以存取null元素，因为系统根据某个位置是否为null来判断元素的存在
- 当作为栈使用时，性能比Stack好；当作为队列使用时，性能比LinkedList好。 