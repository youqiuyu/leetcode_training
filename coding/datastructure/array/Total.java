package array;

import java.util.Arrays;
import java.util.Comparator;

public class Total {
    public static String sortSentence2(String s) {
        String[] arr = s.split(" ");
        StringBuilder sb = new StringBuilder();
        Arrays.sort(arr, Comparator.comparingInt(a -> a.charAt(a.length() - 1)));  //  TODO
        for (String value : arr) {
            sb.append(value, 0, value.length() - 1).append(" ");  //  TODO
        }
        return sb.toString().trim();
    }
}
