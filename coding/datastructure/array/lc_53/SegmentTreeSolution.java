/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package array.lc_53;

public class SegmentTreeSolution {
    public static class Status {
        int lSum;
        int rSum;
        int mSum;
        int iSum;

        public Status(int lSum, int rSum, int mSum, int iSum) {
            this.lSum = lSum;
            this.rSum = rSum;
            this.mSum = mSum;
            this.iSum = iSum;
        }
    }

    public static int maxSubArray(int[] nums) {
        Status resultStatus = calc(nums, 0, nums.length - 1);
        return resultStatus.mSum;
    }

    public static Status calc(int[] nums, int left, int right) {
        if (left == right) {
            return new Status(nums[left], nums[left], nums[left], nums[left]);
        }
        int mid = (left + right) / 2;
        Status leftStatus = calc(nums, left, mid);
        Status rightStatus = calc(nums, mid + 1, right);
        return popUp(leftStatus, rightStatus);
    }

    public static Status popUp(Status left, Status right) {
        int lSum, rSum, mSum, iSum;
        lSum = Math.max(left.lSum, left.iSum + right.lSum);
        rSum = Math.max(right.rSum, left.rSum + right.iSum);
        iSum = left.iSum + right.iSum;
        mSum = Math.max(Math.max(left.mSum, right.mSum), left.rSum + right.lSum);
        return new Status(lSum, rSum, mSum, iSum);
    }

    public static void main(String[] args) {
        int[] test = {-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(maxSubArray(test));
    }
}
