/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package array.lc_53;

import java.util.ArrayList;
import java.util.Arrays;

/*
给定一个整数数组 nums，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。

示例 1：
输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
输出：6
解释：连续子数组[4,-1,2,1] 的和最大，为6 。

示例 2：
输入：nums = [1]
输出：1

示例 3：
输入：nums = [0]
输出：0

示例 4：
输入：nums = [-1]
输出：-1

示例 5：
输入：nums = [-100000]
输出：-100000

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/maximum-subarray
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    // 贪心算法
    // 依次往后遍历数组，记录total值，如果指针前面的数字之和小于0，那么清空指针之前的数组
    // 时间复杂度O(n)
    public static int maxSubArray1(int[] nums) {
        int result = nums[0];
        int sum = 0;
        for (int num : nums) {
            if (sum > 0) {
                sum += num;
            } else {
                sum = num;
            }
            result = Math.max(result, sum);
        }
        return result;
    }

    //联机算法,线性时间
    public static int maxSubArray2(int[] nums) {
        int n = nums.length;
        int[] sum = new int[n];
        sum[0] = nums[0];
        int result = sum[0];
        for(int i = 1; i < nums.length; ++i) {
            sum[i] = nums[i] + Math.max(sum[i - 1], 0);
            result = Math.max(sum[i], result);
        }
        return result;
    }

    // 动态规划，如果前面一个数>0，就把前一个数的值加到当前数上
    // 因为涉及到排序，所以是nlogn
    public static int maxSubArray3(int[] nums) {
        int n = nums.length;
        for(int i = 1; i < n; ++i) {
            if (nums[i - 1] > 0) {
                nums[i] = nums[i - 1] + nums[i];
            }
        }
        Arrays.sort(nums);
        return nums[n - 1];
    }

    public static void main(String[] args) {
        int[] test = {-2,1,-3,4,-1,2,1,-5,4};
//        System.out.println(maxSubArray1(test));
        System.out.println(maxSubArray2(test));
        System.out.println(maxSubArray3(test));
    }
}
