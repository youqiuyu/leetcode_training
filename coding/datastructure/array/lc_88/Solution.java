/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package array.lc_88;

import java.util.Arrays;

/*
给你两个有序整数数组nums1 和 nums2，请你将 nums2 合并到nums1中，使 nums1 成为一个有序数组。
初始化nums1 和 nums2 的元素数量分别为m 和 n 。你可以假设nums1 的空间大小等于m + n，这样它就有足够的空间保存来自 nums2 的元素。


示例 1：

输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
输出：[1,2,2,3,5,6]
示例 2：

输入：nums1 = [1], m = 1, nums2 = [], n = 0
输出：[1]

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/merge-sorted-array
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        System.arraycopy(nums2, 0, nums1, m, n);
        Arrays.sort(nums1);
    }

    // 双指针方法
    public static void merge2(int[] nums1, int m, int[] nums2, int n) {
        int[] result = new int[m + n];
        int f1 = 0;
        int f2 = 0;
        int current;
        while (f1 < m || f2 < n) {
            if (f1 == m) {
                current = nums2[f2++];
            } else if (f2 == n) {
                current = nums1[f1++];
            } else {
                if (nums1[f1] <= nums2[f2]) {
                    current = nums1[f1++];
                } else {
                    current = nums2[f2++];
                }
            }
            result[f1 + f2 -1] = current;
        }
        System.arraycopy(result, 0, nums1, 0, m + n);

    }

    public static void main(String[] args) {
        int[] testArrayA = {1,2,3,0,0,0};
        int[] testArrayB = {2,5,6};
        merge2(testArrayA, 3, testArrayB, 3);
        System.out.println("Finished");
    }
}
