/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.lc_1536;

import java.util.Arrays;
import java.util.LinkedList;

/*
给你一个nxn的二进制网格grid，每一次操作中，你可以选择网格的相邻两行进行交换。
一个符合要求的网格需要满足主对角线以上的格子全部都是0。
请你返回使网格满足要求的最少操作次数，如果无法使网格符合要求，请你返回-1。
主对角线指的是从(1, 1)到(n, n)的这些格子。

示例 1：
输入：grid = [[0,0,1],[1,1,0],[1,0,0]]
输出：3
示例 2：

输入：grid = [[0,1,1,0],[0,1,1,0],[0,1,1,0],[0,1,1,0]]
输出：-1
解释：所有行都是一样的，交换相邻行无法使网格符合要求。
示例 3：

输入：grid = [[1,0,0],[1,1,0],[1,1,1]]
输出：0

提示：
n == grid.length
n == grid[i].length
1 <=  n <= 200
grid[i][j]要么是0要么是1。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/minimum-swaps-to-arrange-a-binary-grid
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static int minSwaps(int[][] grid) {  // 记录每一行最后结尾连续多少个0，方便删除和插入数据，使用链表
        int len = grid.length;
        LinkedList<Integer> calcRowZeroNum = new LinkedList<>();
        for (int[] ints : grid) {
            int count = 0;
            for (int j = len - 1; j >= 0; j--) {
                if (ints[j] == 0) {
                    count++;
                } else {
                    break;
                }
            }
            calcRowZeroNum.add(count);
        }
        int res = 0;
        for (int i = 0; i < len - 1; i++) {  // 第i行至少要n-i-1个零
            for (int j = 0; j < len; j++) {
                if (calcRowZeroNum.get(j) >= len - i - 1) {
                    res = res + j - i;
                    calcRowZeroNum.remove(j);
                    calcRowZeroNum.add(0, 0);
                    break;
                } else {
                    if (j == len - 1) {
                        return -1;
                    }
                }
            }
        }
        return res;
    }

    public static int minSwaps2(int[][] grid) {//先初始化一个属组，用于保存每一行最后一个1的位置; lastOneIndex[i]表示第i行最后一个1的index
        int len = grid.length;
        int[] lastOneIndex = new int[len];
        Arrays.fill(lastOneIndex, 0);





        for (int i = 0; i < len; ++i) {
            for (int j = len - 1; j >= 0; --j) {
                if (grid[i][j] == 1) {
                    lastOneIndex[i] = j;
                    break;
                }
            }
        }
        int res = 0;
        // 要保证lastOneIndex[i] <= i
        for (int i = 0; i < len; ++i) {
            for (int j = i; j < len; ++j) {
                if (lastOneIndex[j] <= i) {
                    res = res + j - i;
                    if (j > i) {
                        for (int k = j; k > i; --k) {
                            int tmp = lastOneIndex[k];
                            lastOneIndex[k] = lastOneIndex[k - 1];
                            lastOneIndex[k - 1] = tmp;
                        }
                    }
                    break;
                } else {
                    if (j == len - 1) {
                        return -1;
                    }
                }
            }
        }
        return res;
    }

        public static void main(String[] args) {
        //int[][] grid1 = {{1,0,0},{1,1,0},{1,1,1}};
        int[][] grid2 = {{0,0,1},{1,1,0},{1,0,0}};
        //System.out.println(minSwaps(grid1));
        System.out.println(minSwaps(grid2));
        System.out.println(minSwaps2(grid2));
    }
}
