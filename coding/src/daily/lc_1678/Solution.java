/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */
/*
 * 请你设计一个可以解释字符串 command 的 Goal 解析器 。command 由 "G"、"()" 和/或 "(al)" 按某种顺序组成。
 * Goal 解析器会将 "G" 解释为字符串 "G"、"()" 解释为字符串 "o" ，"(al)" 解释为字符串 "al" 。
 * 然后，按原顺序将经解释得到的字符串连接成一个字符串。
 * 给你字符串 command ，返回 Goal 解析器 对 command 的解释结果。
 *
 * 示例 1：
 * 输入：command = "G()(al)"
 * 输出："Goal"
 * 解释：Goal 解析器解释命令的步骤如下所示：
 * G -> G
 * () -> o
 * (al) -> al
 * 最后连接得到的结果是 "Goal"
 *
 * 示例 2：
 * 输入：command = "G()()()()(al)"
 * 输出："Gooooal"
 *
 * 示例 3：
 * 输入：command = "(al)G(al)()()G"
 * 输出："alGalooG"
 *
 * 提示：
 * 1 <= command.length <= 100
 * command 由 "G"、"()" 和/或 "(al)" 按某种顺序组成
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/goal-parser-interpretation
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 */
package daily.lc_1678;

public class Solution {
    public static String interpret3(String command) {
        return command.replace("()", "o").replace("(al)", "al");
    }

    // 迭代方法，效率更好
    public static String interpret2(String command) {
        StringBuilder result = new StringBuilder();
        int index = 0;
        while (index < command.length()) {
            if (command.charAt(index) == 'G') {
                result.append("G");
                index++;
            }
            if (command.startsWith("()", index)) {
                result.append("o");
                index += 2;
            }
            if (command.startsWith("(al)", index)) {
                result.append("al");
                index += 4;
            }
        }
        return result.toString();
    }

    // 递归
    public static String interpret(String command) {
        if ("G".equals(command)) {
            return "G";
        }
        if ("()".equals(command)) {
            return "o";
        }
        if ("(al)".equals(command)) {
            return "al";
        }
        if (command.startsWith("G")) {
            return "G" + interpret(command.substring(1));
        }
        if (command.startsWith("()")) {
            return "o" + interpret(command.substring(2));
        }
        if (command.startsWith("(al)")) {
            return "al" + interpret(command.substring(4));
        }
        return "";
    }

    public static void main(String[] args) {
        String command1 = "G()(al)";
        String command2 = "G()()()()(al)";
        String command3 = "(al)G(al)()()G";
        String command4 = "GGG";
        System.out.println(interpret2(command1));
        System.out.println(interpret2(command2));
        System.out.println(interpret2(command3));
        System.out.println(interpret2(command4));
    }
}
