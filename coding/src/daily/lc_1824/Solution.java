/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.lc_1824;

import java.util.Arrays;

public class Solution {
    public static int minSideJumps(int[] obstacles) {
        int len = obstacles.length;
        // 定义状态：dp[i][j]：到达第i个结点且在第j条道路所需要的最少侧跳数量
        int[][] dp = new int[len + 1][4];
        // 初始化
        for (int i = 1; i <= len; i++) {
            Arrays.fill(dp[i], Integer.MAX_VALUE - 1);
        }
        dp[1][2] = 0;
        dp[1][1] = 1;
        dp[1][3] = 1;
        for (int i = 2; i <= len; i++) {
            // 当前结点上的跑道上没有石头, 则直接可以由同一个跑道跳过来
            if (obstacles[i - 1] != 1)
                dp[i][1] = dp[i - 1][1];
            if (obstacles[i - 1] != 2)
                dp[i][2] = dp[i - 1][2];
            if (obstacles[i - 1] != 3)
                dp[i][3] = dp[i - 1][3];

            // 除了可以从同一跑道的前一个结点跳过来, 还可以从其他另外两个跑道侧跳过来
            if (obstacles[i - 1] != 1)
                dp[i][1] = Math.min(dp[i][1], Math.min(dp[i][2], dp[i][3]) + 1);
            if (obstacles[i - 1] != 2)
                dp[i][2] = Math.min(dp[i][2], Math.min(dp[i][1], dp[i][3]) + 1);
            if (obstacles[i - 1] != 3)
                dp[i][3] = Math.min(dp[i][3], Math.min(dp[i][1], dp[i][2]) + 1);
        }

        return Math.min(dp[len][1], Math.min(dp[len][2], dp[len][3]));
    }

    public static void main(String[] args) {
        int[] test = {0,1,2,3,0};
        System.out.println(minSideJumps(test));
    }
}
