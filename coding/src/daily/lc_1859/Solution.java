package daily.lc_1859;

import java.util.Arrays;
import java.util.Comparator;

/*
一个 句子指的是一个序列的单词用单个空格连接起来，且开头和结尾没有任何空格。每个单词都只包含小写或大写英文字母。
我们可以给一个句子添加 从 1 开始的单词位置索引 ，并且将句子中所有单词打乱顺序。
比方说，句子"This is a sentence"可以被打乱顺序得到"sentence4 a3 is2 This1"或者"is2 sentence4 This1 a3"。
给你一个 打乱顺序的句子s，它包含的单词不超过9个，请你重新构造并得到原本顺序的句子。

示例 1：
输入：s = "is2 sentence4 This1 a3"
输出："This is a sentence"
解释：将 s 中的单词按照初始位置排序，得到 "This1 is2 a3 sentence4" ，然后删除数字。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/sorting-the-sentence
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static String sortSentence(String s) {
        String[] wordArray = new String[9];
        String[] wordsIndex = s.split(" ");
        int n = wordsIndex.length;
        for (String str: wordsIndex) {
            int m = str.length();
            wordArray[str.charAt(m - 1) - 49] = str.substring(0, m - 1);
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            sb.append(wordArray[i]).append(" ");
        }
        return sb.toString().trim();
    }

    public static String sortSentence2(String s) {
        String[] arr = s.split(" ");
        StringBuilder sb = new StringBuilder();
        Arrays.sort(arr, Comparator.comparingInt(a -> a.charAt(a.length() - 1)));
        for (String value : arr) {
            sb.append(value, 0, value.length() - 1).append(" ");
        }
        return sb.toString().trim();
    }

    public static void main(String[] args) {
        String a = "is2 sentence4 This1 a3";
        String result = sortSentence2(a);
        System.out.println(result);
    }
}
