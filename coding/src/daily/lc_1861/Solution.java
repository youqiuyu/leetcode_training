package daily.lc_1861;

public class Solution {
    public static char[][] rotateTheBox(char[][] box) {
        int m = box.length;
        int n = box[0].length;
        char[][] result = new char[n][m];
        for (int i = 0; i < m; ++i) {
            // 分析第一行
            int left = -1;
            int right = 0;
            while (right <= n) {
                if (right == n || box[i][right] == '*') {
                    // right是个障碍物
                    // 统计left和right之间的石头数
                    int stoneNum = 0;
                    for (int k = left + 1; k < right; ++k) {
                        if (box[i][k] == '#') {
                            stoneNum++;
                        }
                    }
                    for (int x = right - 1; x >= right - stoneNum; x--) {
                        box[i][x] = '#';
                    }
                    for (int y = left + 1; y < right - stoneNum; y++) {
                        box[i][y] = '.';
                    }
                    left = right;
                }
                right++;
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                result[j][i] = box[m - i - 1][j];
            }
        }
        return result;
    }


    public static void main(String[] args) {
        char[][] testA = {
                {'#', '#', '*', '.', '*', '.'},
                {'#', '#', '#', '*', '.', '.'},
                {'#', '#', '#', '.', '#', '.'}
        };
        rotateTheBox(testA);
        System.out.println("Finished");
    }
}
