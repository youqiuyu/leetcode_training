/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.lc_1902;

import java.nio.charset.StandardCharsets;
import java.util.*;


/**
 * OJ考题代码：公共字符
 *
 * @author 命题组
 * @since 2021-02-04
 */

public class Main {
    /**
     * main入口由OJ平台调用
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in, StandardCharsets.UTF_8.name());
        int nValue = cin.nextInt();
        int mValue = cin.nextInt();
        cin.nextLine();
        String[] strings = new String[mValue];
        for (int i = 0; i < mValue; i++) {
            strings[i] = cin.nextLine();
        }
        cin.close();

        char[] results = getNTimesCharacter(nValue, strings);

        System.out.print("[");
        for (int i = 0; i < results.length; i++) {
            if (i == 0) {
                System.out.print(results[i]);
            } else {
                System.out.print(" " + results[i]);
            }
        }
        System.out.print("]");
    }

    // 待实现函数，在此函数中填入答题代码
    private static char[] getNTimesCharacter(int nValue, String[] strings) {
        int n = strings.length;
        Set<Character> result = new HashSet<>();
        String first = strings[0];
        for (int i = 0; i < first.length(); ++i) {
            int count = 0;
            for (int j = 0; j < first.length(); ++j) {
                if (first.charAt(i) == first.charAt(j)) {
                    count++;
                }
            }
            if (count >= nValue) {
                result.add(first.charAt(i));
            }
        }
        return getChars(nValue, strings, n, result);
    }

    private static char[] getChars(int nValue, String[] strings, int n, Set<Character> result) {
        for (int i = 1; i < n; ++i) {
            String newStr = strings[i];
            Map<Character, Integer> map = new HashMap<>();
            for (Character c : result) {
                int num = 0;
                for (int j = 0; j < newStr.length(); ++j) {
                    if (newStr.charAt(j) == c) {
                        num++;
                    }
                }
                map.put(c, num);
            }
            for (Character c : map.keySet()) {
                if (map.get(c) < nValue) {
                    result.remove(c);
                }
            }
        }
        return getRes(result);
    }

    private static char[] getRes(Set<Character> result) {
        if (result.size() == 0) {
            return new char[0];
        }
        List<Character> a = new ArrayList<>();
        a.addAll(result);
        Collections.sort(a);
        char[] res = new char[a.size()];
        for (int i = 0; i < a.size(); ++i) {
            res[i] = a.get(i);
        }
        return res;
    }
}