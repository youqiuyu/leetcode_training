/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.lc_300;

import java.util.Arrays;
import java.util.OptionalInt;

/*
给你一个整数数组 nums ，找到其中最长严格递增子序列的长度。

子序列是由数组派生而来的序列，删除（或不删除）数组中的元素而不改变其余元素的顺序。例如，[3,6,2,7] 是数组 [0,3,1,6,2,2,7] 的子序列。

示例 1：
输入：nums = [10,9,2,5,3,7,101,18]
输出：4
解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。

示例 2：
输入：nums = [0,1,0,3,2,3]
输出：4

示例 3：
输入：nums = [7,7,7,7,7,7,7]
输出：1

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/longest-increasing-subsequence
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static int lengthOfLIS(int[] nums) {
        int[] tmp = new int[nums.length];
        int result = 1;
        for (int i = 0; i < nums.length; ++i) {
            result = Math.max(result, curIndexRes(nums, i, tmp));
        }
        return result;
    }

    // 计算以index为下标开头的数的结果
    public static int curIndexRes(int[] nums, int index, int[] tmp) {
        if (tmp[index] != 0) {
            return tmp[index];
        }
        int n = nums.length;
        if (index == n - 1) {
            tmp[index] = 1;
            return 1;
        }
        int result = 1;
        for (int i = index + 1; i < n; ++i) {
            if (nums[i] > nums[index]) {
                result = Math.max(result, curIndexRes(nums, i, tmp) + 1);
            }
        }
        tmp[index] = result;
        return tmp[index];
    }


    public static int lengthOfLIS1(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = 1;
        int res = dp[0];
        for (int i = 1; i < nums.length; ++i) {
            dp[i] = 1;
            for (int j = 0; j < i; ++j) {
                if (nums[i] > nums[j]) dp[i] = Math.max(dp[i], dp[j] + 1);
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }

    public Integer lengthOfLIS2(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = 1;
        for (int i = 1; i < nums.length; ++i) {
            dp[i] = 1;
            for (int j = 0; j < i; ++j) {
                if (nums[i] > nums[j]) dp[i] = Math.max(dp[i], dp[j] + 1);
            }
        }
        return Arrays.stream(dp).max().getAsInt();
    }


        public int lengthOfLIS3(int[] nums) {
            int len = nums.length;
            if (len <= 1) {
                return len;
            }
            int[] tail = new int[len];
            tail[0] = nums[0];
            int end = 0;

            for (int i = 1; i < len; i++) {
                if (nums[i] > tail[end]) {
                    end++;
                    tail[end] = nums[i];
                } else {
                    int left = 0;
                    int right = end;
                    while (left < right) {
                        int mid = left + ((right - left) >>> 1);
                        if (tail[mid] < nums[i]) {
                            left = mid + 1;
                        } else {
                            right = mid;
                        }
                    }
                    tail[left] = nums[i];
                }
            }
            end++;
            return end;
        }




    public static void main(String[] args) {
//        int[] test1 = {10,9,2,5,3,7,101,18};
//        int[] test2 = {0,1,0,3,2,3};
//        int[] test3 = {7,7,7,7,7,7,7};
        int[] test4 = {2,15,3,7,8,6,18};
        int[] test5 = {1,3,6,7,9,4,10,5,6};
//        System.out.println(lengthOfLIS(test1));
//        System.out.println(lengthOfLIS(test2));
//        System.out.println(lengthOfLIS(test3));
//        System.out.println(lengthOfLIS(test4));
        System.out.println(lengthOfLIS1(test5));
    }
}
