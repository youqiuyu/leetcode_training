/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.lc_300;

public class Solution2 {
    public int lengthOfLIS(int[] nums) {
        // 定义一个数组叫tail[i],表示长度为i+1的递增子序列中，结尾最小的值。
        int n = nums.length;
        int[] tail = new int[n];
        int end = 0; // 当前tail已经赋值的长度
        if (n == 1) {
            return 1;
        }
        tail[0] = nums[0];
        for (int i = 1; i < n; ++i) {
            if (nums[i] > tail[end]) {
                tail[++end] = nums[i];
            } else {
                int left = 0;
                int right = end;
                while (left < right) {
                    int mid = left + ((right - left) / 2);
                    if (tail[mid] < nums[i]) {
                        left = mid + 1;
                    } else {
                        right = mid;
                    }
                }
                tail[left] = nums[i];
            }
        }
        return ++end;
    }
}
