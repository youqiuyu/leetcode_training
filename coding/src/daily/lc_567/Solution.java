/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.lc_567;

import java.util.ArrayList;
import java.util.Arrays;

/*
给你两个字符串s1和s2 ，写一个函数来判断 s2 是否包含 s1的排列。
换句话说，s1 的排列之一是 s2 的 子串 。

示例 1：
输入：s1 = "ab" s2 = "eidbaooo"
输出：true
解释：s2 包含 s1 的排列之一 ("ba").

示例 2：
输入：s1= "ab" s2 = "eidboaoo"
输出：false

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/permutation-in-string
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static boolean checkInclusion2(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        if (m > n) {
            return false;
        }
        int[] s1Num = new int[26];
        int[] s2Num = new int[26];
        for (int i = 0; i < s1.length(); ++i) {
            s1Num[s1.charAt(i) - 'a']++;
            s2Num[s2.charAt(i) - 'a']++;
        }
        if (Arrays.equals(s1Num, s2Num)) {
            return true;
        }
        for (int i = 0; i <= n - m - 1; ++i) {
            s2Num[s2.charAt(i)  - 'a']--;
            s2Num[s2.charAt(i + m - 1)  - 'a']++;
            if (Arrays.equals(s1Num, s2Num)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkInclusion4(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        if (m > n) {
            return false;
        }
        int[] cnt = new int[26];
        for (int i = 0; i < m; ++i) {
            --cnt[s1.charAt(i) - 'a'];
        }
        int left =  0;
        for (int right = 0; right < n; ++right) {
            int cur = s2.charAt(right) - 'a';
            ++cnt[cur];
            while (cnt[cur] > 0) {
                --cnt[s2.charAt(left) - 'a'];
                ++left;
            }
            if (right - left + 1 == m) {
                return true;
            }
        }
        return false;
    }



    public static boolean checkInclusion(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        if (m > n) {
            return false;
        }
        char[] s1Char = s1.toCharArray();
        Arrays.sort(s1Char);
        String s1Sorted = String.valueOf(s1Char);
        // s2往右挪动
        for (int i = 0; i <= n - m; ++i) {
            String curStr = s2.substring(i, i + m);
            char[] s2Char = curStr.toCharArray();
            Arrays.sort(s2Char);
            String s2Sorted = String.valueOf(s2Char);
            if (s1Sorted.equals(s2Sorted)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkInclusion1(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        if (m > n) {
            return false;
        }
        int[] lettersNum = str2Array(s1);
        // s2往右挪动
        for (int i = 0; i <= n - m; ++i) {
            String curStr = s2.substring(i, i + m);
            int[] lettersNumCur = str2Array(curStr);
            if (Arrays.equals(lettersNum, lettersNumCur)) {
                return true;
            }
        }
        return false;
    }

    public static int[] str2Array (String s1) {
        int[] lettersNum = new int[26];
        for (int i = 0; i < s1.length(); ++i) {
            lettersNum[s1.charAt(i) - 'a']++;
        }
        return lettersNum;
    }


    public static boolean checkInclusion3(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        if (m > n) {
            return false;
        }
        int tmp = hash_code(s1);
        // s2往右挪动
        for (int i = 0; i <= n - m; ++i) {
            String curStr = s2.substring(i, i + m);
            if (hash_code(curStr) == tmp) {
                return true;
            }
        }
        return false;
    }

    public static int hash_code(String s) {
        char[] sChar = s.toCharArray();
        int sum = 0;
        for (char aChar : sChar) {
            sum+=((int)aChar)*((int)aChar);
        }
        return sum;
    }





    public static void main(String[] args) {
        System.out.println(checkInclusion("abcdef", "bsssssss"));
        System.out.println(checkInclusion("abcdef", "addcbafegh"));
    }
}
