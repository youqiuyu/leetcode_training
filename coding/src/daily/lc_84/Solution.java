/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.lc_84;

import java.util.*;

public class Solution {
    public static int largestRectangleArea2(int[] heights){
        int res = 0;
        int len = heights.length;
        if (len == 0) {
            return 0;
        }
        if (len == 1) {
            return heights[0];
        }
        int[] newHeights = new int[len + 2];
        newHeights[0] = 0;
        System.arraycopy(heights, 0, newHeights, 1, len);
        newHeights[len + 1] = 0;
        len += 2;
        heights = newHeights;
        Deque<Integer> stack = new ArrayDeque<>(len); // 用于记录栈内容
        // 先放入哨兵，在循环里就不用做非空判断
        stack.addLast(0);
        for (int i = 1; i < len; i++) {
            while (heights[i] < heights[stack.peekLast()]) {
                int curHeight = heights[stack.pollLast()];
                int curWidth = i - stack.peekLast() - 1;
                res = Math.max(res, curHeight * curWidth);
            }
            stack.addLast(i);
        }
        return res;
    }

    public static int largestRectangleArea(int[] heights) { // 超时
        int res = 0;
        int n = heights.length;
        for (int i = 1; i <= n; ++i) {
            for (int j = 0; j < n - i + 1; ++j) {
                if (i == 1) {
                    res = Math.max(res, heights[j]);
                } else {
                    int min = heights[j];
                    for (int k = j; k < j + i; ++k) {
                        min = Math.min(min, heights[k]);
                    }
                    res = Math.max(res, i * min);
                }

            }
        }
        return res;
    }

    /*
    我们可以遍历每根柱子，以当前柱子 i 的高度作为矩形的高，
    那么矩形的宽度边界即为向左找到第一个高度小于当前柱体 i 的柱体，
    向右找到第一个高度小于当前柱体 i 的柱体。
     */

    public static int largestRectangleArea1(int[] heights) { // 超时
        int res = 0;
        int n = heights.length;
        for (int i = 0 ; i < n; ++i) {
            int leftNum = 0;
            int rightNum = 0;
            for (int j = i + 1; j < n; ++j) {
                if (heights[j] < heights[i]) {
                    rightNum = j - i - 1;
                    break;
                } else {
                    if (j == n - 1) {
                        rightNum = j - i;
                    }
                }
            }
            for (int j = i - 1; j >= 0; --j) {
                if (heights[j] < heights[i]) {
                    leftNum = i - j - 1;
                    break;
                } else {
                    if (j == 0) {
                        leftNum = i - j;
                    }
                }
            }
            res = Math.max(res, heights[i] * (leftNum + rightNum + 1));
        }
        return res;
    }

    public static void main(String[] args) {
        int[] test = {2, 1, 5, 6, 2, 3};
        int[] test2 = {2, 4};
        System.out.println(largestRectangleArea1(test));
        System.out.println(largestRectangleArea1(test2));

    }
}
