/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.oj_1787;

import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * OJ考题代码：最长元音子串
 *
 * @author 命题组
 * @since 2020-3-3
 */
public class Main {
    /**
     * main入口由OJ平台调用
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in, StandardCharsets.UTF_8.name());
        String input = cin.nextLine();
        cin.close();

        System.out.println(getLongestVowelStringLength(input));
    }

    // 待实现函数，在此函数中填入答题代码
    private static int getLongestVowelStringLength(String input) {
        String defaultVowel = "aeiouAEIOU";
        int result = 0;
        Deque<Character> deque = new LinkedList<>();
        for (int i = 0; i < input.length(); ++i) {
            result = Math.max(result, deque.size());
            if (defaultVowel.contains(Character.toString(input.charAt(i)))) {
                deque.offerFirst(input.charAt(i));
            } else {
                deque.clear();
            }
        }
        result = Math.max(result, deque.size());
        return result;
    }

    private static int getLongestVowelStringLength2(String input) {
        String vowels = "aeiouAEIOU";
        String[] strings = input.split("");
        int result = 0;
        int sum = 0;
        for (String s : strings) {
            if (vowels.contains(s)) {
                sum++;
                result = Math.max(result, sum);
            } else {
                sum = 0;
            }
        }
        return result;
    }
}