/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.oj_1788;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * OJ考题代码：计算面积
 *
 * @author 命题组
 * @since 2020-03-20
 */

public class Main {
    /**
     * main入口由OJ平台调用
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in, StandardCharsets.UTF_8.name());
        int row = cin.nextInt();
        int stopPoint = cin.nextInt();
        int[][] operations = new int[row][2];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < 2; j++) {
                operations[i][j] = cin.nextInt();
            }
        }
        cin.close();
        long result = getMinArea(stopPoint, operations);
        System.out.println(result);
    }

    // 待实现函数，在此函数中填入答题代码
    private static long getMinArea(int stopPoint, int[][] operations) {
        // 计算纵坐标累加的结果集，在原始坐标进行累加
        for (int i = 1; i < operations.length; i++) {
            operations[i][1] = operations[i][1] + operations[i - 1][1];
        }
        long area = 0L;
        // 计算面积：(x，y) (a,b)=>(x-a)*y
        for (int i = 1; i < operations.length; i++) {
            area += Math.abs(operations[i][0] - operations[i - 1][0]) * Math.abs(operations[i - 1][1]);
        }
        // 最后坐标算进去
        area +=
                Math.abs(stopPoint - operations[operations.length - 1][0]) * Math.abs(operations[operations.length - 1][1]);
        return area;
    }
}

