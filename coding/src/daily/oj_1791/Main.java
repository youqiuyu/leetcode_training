/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.oj_1791;


/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 * Description: 上机编程认证
 * Note: 提供的缺省代码仅供参考，可自行根据答题需要进行使用、修改或删除。
 */

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * OJ考题代码：设备编号
 *
 * @author 命题组
 * @since 2020-3-3
 */
public class Main {
    /**
     * main入口由OJ平台调用
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in, StandardCharsets.UTF_8.name());
        int start = cin.nextInt();
        int end = cin.nextInt();
        cin.close();

        System.out.println(getNormalDeviceNum(start, end));
    }

    // 待实现函数，在此函数中填入答题代码
    private static int getNormalDeviceNum(int start, int end) {
        int result = end - start + 1;
        for (int i = start; i <= end; ++i) {
            String i2Str = String.valueOf(i);
            for (int j = 0; j < i2Str.length(); ++j) {
                if (i2Str.charAt(j) == '4') {
                    result--;
                    break;
                }
                if (j != i2Str.length() - 1 && i2Str.charAt(j) == '1' && i2Str.charAt(j + 1) == '8') {
                    result--;
                    break;
                }
            }
        }
        return result;
    }

    private static int getNormalDeviceNum2(int start, int end) {
        int result = end - start + 1;
        for (int i = start; i <= end; ++i) {
            String i2Str = String.valueOf(i);
            if (i2Str.contains("4") || i2Str.contains("18")) {
                result--;
            }
        }
        return result;
    }

    private static int getNormalDeviceNum3(int start, int end) {
        int num = 0;
        for (int i = start; i <= end; i++) {
            String strnum = Integer.toString(i);
            if (strnum.indexOf("4") >= 0 || strnum.indexOf("18") >= 0) {
                num++;
            }
        }
        return end - start + 1 - num;
    }
}
