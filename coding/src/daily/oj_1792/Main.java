/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.oj_1792;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

/**
 * OJ考题代码：服务器集群网络延迟
 *
 * @author 命题组
 * @since 2020-03-19
 */

public class Main {
    /**
     * main入口由OJ平台调用
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in, StandardCharsets.UTF_8.name());
        int line = cin.nextInt();
        int[] input = new int[line];
        for (int i = 0; i < line; i++) {
            input[i] = cin.nextInt();
        }
        cin.close();
        int result = clusterLatency(input);
        System.out.println(result);
    }

    // 待实现函数，在此函数中填入答题代码
    private static int clusterLatency(int[] arr) {
        int result = Integer.MAX_VALUE;
        for (int j : arr) {
            int currentRes = 0;
            for (int k : arr) {
                currentRes += Math.abs(j - k);
            }
            result = Math.min(result, currentRes);
        }
        return result;
    }

    private static int clusterLatency2(int[] arr) {
        Arrays.sort(arr);
        int n = arr.length;
        int middle = arr[n / 2];
        int result = 0;
        for (int j : arr) {
            result += Math.abs(j - middle);
        }
        return result;
    }
}

