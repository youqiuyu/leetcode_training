/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.oj_1793;

import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

/**
 * OJ考题代码：满足差为s的组合
 *
 * @author 命题组
 * @since 2020-03-20
 */
public class Main {
    /**
     * main入口由OJ平台调用
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in, StandardCharsets.UTF_8.name());
        int diff = cin.nextInt();
        int line = cin.nextInt();
        int[] arr = new int[line];
        for (int i = 0; i < line; i++) {
            arr[i] = cin.nextInt();
        }
        cin.close();
        int result = proc2(arr, diff);
        System.out.println(result);
    }

    // 待实现函数，在此函数中填入答题代码
    // 超时
    private static int proc(int[] arr, int diff) {
        if (diff == 0) {
            return 0;
        }
        int result = 0;
        for (int k : arr) {
            for (int i : arr) {
                if (Math.abs(k - i) == Math.abs(diff)) {
                    result++;
                }
            }
        }
        return result / 2;
    }

    private static int proc2(int[] arr, int diff) {
        if (diff == 0) {
            return 0;
        }
        int n = arr.length;
        int result = 0;
        Arrays.sort(arr);
        int left = 0;
        int right = 0;
        while (left < n && right < n) {
            if (Math.abs(arr[left] - arr[right]) == Math.abs(diff)) {
                result++;
                left++;
            }
            right++;
        }
        return result ;
    }
}
