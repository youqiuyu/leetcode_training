/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.oj_1813;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;

/**
 * OJ考题代码：用给定的数字组成IP地址
 *
 * @author 命题组
 * @since 2020-3-3
 */
public class Main {
    /**
     * main入口由OJ平台调用
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in, StandardCharsets.UTF_8.name());
        String nums = cin.nextLine();
        cin.close();

        System.out.println(composeIpAddress(nums));
    }

    static long composeIpAddress(String nums) {
        int oneNum = 0;
        int twoNum = 0;
        int tressNum = 0;
        int n = nums.length();
        String[] arr = nums.split("");
        int[] array = new int[n];
        for (int i = 0; i < n; ++i) {
            array[i] = Integer.parseInt(arr[i]);
        }
        Arrays.sort(array);
        oneNum = n;
        if (array[0] == 0) {
            twoNum = n * (n - 1);
        } else {
            twoNum = n * n;
        }
        for (int i : array) {
            if (i != 0) {
                for (int j : array) {
                    for (int k : array) {
                        int current = i * 100 + j * 10 + k;
                        if (current <= 255) {
                           tressNum++;
                        }
                    }
                }
            }
        }
        return (long) Math.pow((oneNum + twoNum + tressNum), 4);
    }
}
