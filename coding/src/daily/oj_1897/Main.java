/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package daily.oj_1897;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * OJ考题代码：促销活动
 *
 * @author 命题组
 * @since 2021-01-30
 */

public class Main {
    /**
     * main入口由OJ平台调用
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in, StandardCharsets.UTF_8.name());
        int row = cin.nextInt();
        cin.nextLine();
        String[] orderTime = new String[row];
        for (int i = 0; i < row; i++) {
            orderTime[i] = cin.nextLine();
        }
        cin.close();
        int result = freeOrder(orderTime);
        System.out.print(result);
    }

    // 待实现函数，在此函数中填入答题代码
    private static int freeOrder(String[] orderTime) {
        int result = 0;
        Map<String, List<String>> map = new HashMap<>();
        for (String order : orderTime) {
            String orderSecond = order.substring(0, 19);
            String orderMill = order.substring(20, 23);
            if (map.containsKey(orderSecond)) {
                List<String> orderMillList = map.get(orderSecond);
                orderMillList.add(orderMill);
            } else {
                List<String> list = new ArrayList<>();
                list.add(orderMill);
                map.put(orderSecond, list);
            }
        }
        for (String i : map.keySet()) {
            List<String> minInSecond = map.get(i);
            Collections.sort(minInSecond);
            for (String k : minInSecond) {
                if (k.equals(minInSecond.get(0))) {
                    result++;
                }
            }
        }
        return result;
    }
}

